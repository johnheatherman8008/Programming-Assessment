Johnathan Thomas Heatherman

Junior Developer Assessment

Java Version:
    java 16.0.2 2021-07-20
    Java(TM) SE Runtime Environment (build 16.0.2+7-67)

OS:
    NAME="Debian GNU/Linux"
    VERSION_ID="10"
    VERSION="10 (buster)"
    
How To Run:
    1. Unpack ZipFile into Folder or "Extract Here"
    2. Open CMD/Terminal and Run "java -jar JDA.jar <filename>.txt"
  Or
    1. Open CMD/Terminal & Run "javac Main.Java"
    2. then "java Main <filename>.txt"

