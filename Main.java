
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


public class Main {
	/*Put the List Arrays & Maps (Java equivalent to a Python Dictionary) up here so i don't need to pass them anywhere*/
	private static ArrayList<String> Trips         = new ArrayList<String>();
	private static Map<String, Integer> distance   = new TreeMap<String, Integer>();
	private static Map<String, Integer> time       = new HashMap<String, Integer>();
	private static TreeMap<Integer, String> Order  = new TreeMap<Integer, String>(new DesOrder()); // For a sorted Map
	
	public static void main(String[] args) {
		/* Main method */
		File file;
		file = new File(args[0]);
		if (file.exists()) {
			try {
				fileReader(file);
				inputManipulator();
				printINOrder();
				
			} catch (IOException | ParseException e) {
				System.exit(0);
			}
			
		} else {
			System.out.println("File Not Found");
			System.out.println("Please check correct path from current Working Directory");
			System.exit(0);

		}
	}
	
	
	private static void fileReader(File file) throws IOException, ParseException { 
		BufferedReader br;
		String ln = "";
		br = new BufferedReader(new FileReader(file)); 
		
		while (true) {
			ln = br.readLine();
			if (ln==null) {break;}/*Tried to make this the while condition and would not work */	
			
			/*Finds the command in each line */
			else if (ln.contains("Driver")) {
				ln = ln.substring(7);
				time.put(ln, 0);
				distance.put(ln, 0);
				ln = "";	
			}
			else if (ln.contains("Trip")) {
				ln = ln.substring(5);
				Trips.add(ln);
				ln = "";
			}
			else {
				continue;
			}
		}
		
		br.close(); // Close BufferedReader
	}
	
	
	
	private static void inputManipulator() throws ParseException {
		for (String Trip : Trips) {
			String[] trip = Trip.split(" ");
			checker(trip);
			}
		for(String key : time.keySet()) {
			String str = outputProducer(key); //Format the Output Line
			Order.put(distance.get(key), str);
		}
	}
	
	
		
	private static void checker(String[] trip) throws ParseException { 
		
		/*Checks for doubles and adds them together */ 
		SimpleDateFormat format = new SimpleDateFormat("HH:mm");
		for (String driver : time.keySet()) {
			if (driver.equals(trip[0])) {
				Date date1 = format.parse(trip[1]);
				Date date2 = format.parse(trip[2]);
				int totalTime = (int) (date2.getTime() - date1.getTime());
				totalTime = totalTime/1000/60; //convert to minutes
				double mph = (Double.parseDouble(trip[3]) / (totalTime/60)); // MPH
				
				if ((mph < 100) || (mph > 5)) {
					int NT = ((int)time.get(driver) + totalTime);
					time.put(driver, NT);
					int dist = ((int) distance.get(driver) + 
							(Math.round(Float.parseFloat(trip[3]))));
					distance.put(driver, dist);	
				}else {
					
					continue;
				}
			}
		}
	}
	
	
	
	private static String outputProducer(String key) {
		int mph;
		String output;
		
		int Distance = (int) distance.get(key);
		long Minute  = time.get(key);
		
		try {
			mph = (int) Math.round((Distance / (Minute/60.0)));
			
			if (mph != 0) {
				output = (key + ": " + Distance + " miles "+ "@ " + mph +" mph");
			}else {
				output = (key + ": " + Distance + " miles"); // No Trips for driver
			}
			
		}catch(ArithmeticException e) {
			//Divided By 0
			mph = 0;
			output = (key + ": " + Distance + " miles");
		}
		
		return output;
	}
	
	
	
	private static void printINOrder() {
		for (Map.Entry<Integer, String> entry : Order.entrySet()) {
           System.out.println(entry.getValue());  
		}	   
	}
}



class DesOrder implements Comparator<Integer> { 
	// Class in order to arrange Map in order by distance (greatest to least)
	@Override
	public int compare(Integer dist1, Integer dist2) {
	    return dist2.compareTo(dist1);
	}
}
